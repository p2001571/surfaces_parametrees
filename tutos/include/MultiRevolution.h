#pragma once
#include "Revolution.h"

class MultiRevolution
{
private:
    std::vector<Point> points_controle;
    std::vector<Revolution> revolutions;
    const unsigned int nb_points_controle = 100;
    const unsigned int nb_courbes = 10;

    GLuint vao;
    GLuint vbo_vertices;
    GLuint vbo_normals;
    GLuint shader_program;
    std::vector<vec3> vertices_triangle;
    std::vector<vec3> normals;

public:
    MultiRevolution()
    {
        initRevolutions();
        initBuffers();
    }

    void initRevolutions()
    {
        for (int i = 0; i < nb_points_controle; i++)
        {
            float x = cos((float)i / 10) * 10;
            float y = sin((float)i / 10) * 10;
            float z = (float)i / 3;
            points_controle.push_back(vec3(x, y, z));
        }

        int nb_points_par_courbes = nb_points_controle / nb_courbes;
        for (int i = 0; i < nb_courbes; i++)
        {
            std::vector<Point> points;

            for (int k = 0; k < nb_points_par_courbes; k++)
            {
                int index_point = (i * nb_points_par_courbes) - i + k;

                if (index_point < 0)
                    index_point = 0;

                points.push_back(points_controle[index_point]);
            }

            revolutions.push_back(Revolution(points));
        }

        for (int i = 0; i < nb_courbes; i++)
            for (int k = 0; k < revolutions[i].vertices_triangle.size(); k++)
            {
                vertices_triangle.push_back(revolutions[i].vertices_triangle[k]);
                normals.push_back(revolutions[i].normals_triangle[k]);
            }
    }

    void initBuffers()
    {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(1, &vbo_vertices);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);

        glBufferData(
            GL_ARRAY_BUFFER,
            vertices_triangle.size() * sizeof(vec3),
            &vertices_triangle.front().x,
            GL_STATIC_DRAW
        );
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);

        // normals
        glGenBuffers(1, &vbo_normals);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);

        glBufferData(
            GL_ARRAY_BUFFER,
            normals.size() * sizeof(vec3),
            &normals.front().x,
            GL_STATIC_DRAW
        );

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        shader_program = read_program("tutos/shader.glsl");
        program_print_errors(shader_program);
    }

    ~MultiRevolution()
    {
        glDeleteVertexArrays(1, &vao);
        glDeleteBuffers(1, &vbo_vertices);
        glDeleteBuffers(1, &vbo_normals);
        release_program(shader_program);
    }

    int getNbPointsControle() { return nb_points_controle; }

    void draw(Transform& vp)
    {
        glUseProgram(shader_program);
        glBindVertexArray(vao);

        program_uniform(shader_program, "vpMatrix", vp);
        glDrawArrays(GL_TRIANGLES, 0, vertices_triangle.size());
    }
};
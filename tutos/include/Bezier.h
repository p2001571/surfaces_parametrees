#pragma once
#include <vector>
#include <chrono>
#include "draw.h"        
#include <uniforms.h>

struct Bezier
{
    static const unsigned int nb_points_x = 15;
    static const unsigned int nb_points_z = 15;
    Point points_controle[nb_points_x][nb_points_z];

    double nb_vertex = 100;
    std::vector<vec3> vertices;
    std::vector<vec3> vertices_triangle;
    std::vector<vec3> normals;
    int combinaisons[15] = { 1, 14, 91, 364, 1001, 2002, 3003, 3432, 3003, 2002, 1001, 364, 91, 14, 1 };

    GLuint vao;
    GLuint vbo_vertices;
    GLuint vbo_normals;
    GLuint shader_program;

    Bezier();

    void initBuffers();

    void draw(Transform& vp);

    ~Bezier();

    Point pointAtuv(double u, double v);

    void polygonize();
};
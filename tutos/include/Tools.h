#pragma once
#include <vec.h>

double factorielle(double n);

double combinaison(double m, double i);

double bernstein(double combinaison_m_i, const double& m, const double& i, const double& u);

Point cross(Point& a, Point& b);

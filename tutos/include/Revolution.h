#pragma once
#include "Courbe.h"
#include <chrono>
#include "draw.h"        
#include "app_camera.h"
#include <uniforms.h>

struct Revolution
{
    Courbe c;
    unsigned int nb_vertex_courbe = 100;
    unsigned int nb_vertex_radial = 100;

    GLuint vao;
    GLuint vbo_vertices;
    GLuint vbo_normals;
    GLuint shader_program;
    std::vector<vec3> vertices_triangle;
    std::vector<vec3> normals_triangle;

    Revolution();

    Revolution(std::vector<Point> points_controle);

    void initBuffers();

    float equationRadialeEtoile(float angle);

    float equationRadialeFleur(float angle);

    float equationRadialeSmooth(float angle);

    void Polygonize();

    void draw(Transform& vp);

    ~Revolution();
};
#pragma once
#include <vector>
#include <vec.h>

struct Courbe
{
    std::vector<Point> points_controle;
    unsigned int nb_points;
    int combinaisons_courbes[10] = { 1, 9, 36, 84, 126, 126, 84, 36, 9, 1 };

    Courbe() : nb_points(0) {}

    Courbe(std::vector<Point> points_controle);

    Point pointAtT(double t);

    Point tangenteAtT(double t);
};
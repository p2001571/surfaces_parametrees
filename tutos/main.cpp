#include "orbiter.h"
#include "app_camera.h"

#include "tutos/include/Tools.h"
#include <tutos/include/Bezier.h>
#include <tutos/include/Revolution.h>
#include <tutos/include/MultiRevolution.h>


// commun au deux sujets
class TP : public AppCamera
{
public:

    TP() : AppCamera(1024, 640) {}

    int init()
    {
        Point end = r.c.points_controle.back() * 2;
        camera().lookat(Point(0, 0, 0), end);

        mr.initRevolutions();

        glClearDepth(1.f);
        glDepthFunc(GL_LESS);
        glEnable(GL_DEPTH_TEST);

        return 0;
    }

    int quit()
    {
        return 0;
    }

    int render()
    {
        glClearColor(0.2f, 0.2f, 0.2f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        Transform view = camera().view();
        Transform projection = camera().projection();
        Transform vp = projection * view;

        b.draw(vp);
        //r.draw(vp);
        //mr.draw(vp);

        return 1;
    }

private:
    Bezier b;
    Revolution r;
    MultiRevolution mr;
};


int main(int argc, char** argv)
{
    srand(time(NULL));
    TP tp;
    tp.run();

    return 0;
}
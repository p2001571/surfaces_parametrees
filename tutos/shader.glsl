#version 330

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

uniform mat4 vpMatrix;
out vec3 color;

void main( )
{
    color = normal;
    gl_Position = vpMatrix * vec4(position, 1);
}

#endif


#ifdef FRAGMENT_SHADER
out vec4 fragment_color;
in vec3 color;

vec3 light = normalize(vec3(1, -1, 0));

void main( )
{
    float cos = dot(normalize(color), light);
    fragment_color = vec4(cos, cos, cos, 1);
}

#endif
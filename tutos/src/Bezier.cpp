#include "../include/Bezier.h"
#include <tutos/include/Tools.h>

Bezier::Bezier()
{
    vertices.resize(nb_vertex * nb_vertex);

    for (int x = 0; x < nb_points_x; x++)
        for (int z = 0; z < nb_points_z; z++)
            points_controle[x][z] = Point((double)x, cos(x) + cos(z), (double)z);

    polygonize();
    initBuffers();

    printf("Surface de Bezier:\n");
    printf("Nb vertex: %d\n", vertices.size());
    printf("Nb vertex triangles: %d\n", vertices_triangle.size());
}

void Bezier::initBuffers()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // for positions
    glGenBuffers(1, &vbo_vertices);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);
    glBufferData(GL_ARRAY_BUFFER, vertices_triangle.size() * sizeof(vec3), &vertices_triangle.front().x, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    // for normals
    glGenBuffers(1, &vbo_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals.front().x, GL_STATIC_DRAW);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    // shader program
    shader_program = read_program("tutos/shader.glsl");
    program_print_errors(shader_program);
}

void Bezier::draw(Transform& vp)
{
    glUseProgram(shader_program);
    glBindVertexArray(vao);

    program_uniform(shader_program, "vpMatrix", vp);
    glDrawArrays(GL_TRIANGLES, 0, vertices_triangle.size());
}

Bezier::~Bezier()
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo_vertices);
    glDeleteBuffers(1, &vbo_normals);
    release_program(shader_program);
}

Point Bezier::pointAtuv(double u, double v)
{
    Point point_uv(0, 0, 0);

    for (int i = 0; i < nb_points_x; i++)
        for (int j = 0; j < nb_points_z; j++)
        {
            double temp1 = bernstein(combinaisons[i], nb_points_x - 1, i, u);
            double temp2 = bernstein(combinaisons[j], nb_points_z - 1, j, v);
            point_uv = point_uv + (temp1 * temp2 * points_controle[i][j]);
        }

    return point_uv;
}

void Bezier::polygonize()
{
    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

    // making the vertices
    for (int x = 0; x < nb_vertex; x++)
        for (int z = 0; z < nb_vertex; z++)
        {
            double u = ((double)x) / (nb_vertex - 1);
            double v = ((double)z) / (nb_vertex - 1);

            Point point_uv = pointAtuv(u, v);
            vertices[(x * nb_vertex) + z] = point_uv;
        }

    // making the triangles
    for (int i = 0; i < nb_vertex - 1; i++)
        for (int j = 0; j < nb_vertex - 1; j++)
        {
            // first triangle
            vertices_triangle.push_back(vertices[(i * nb_vertex) + j]);
            vertices_triangle.push_back(vertices[((i + 1) * nb_vertex) + j]);
            vertices_triangle.push_back(vertices[((i + 1) * nb_vertex) + j + 1]);

            vec3 n = cross(vertices[((i + 1) * nb_vertex) + j] - vertices[(i * nb_vertex) + j],
                vertices[((i + 1) * nb_vertex) + j + 1] - vertices[(i * nb_vertex) + j]);

            normals.push_back(n);
            normals.push_back(n);
            normals.push_back(n);

            // second triangle
            vertices_triangle.push_back(vertices[(i * nb_vertex) + j]);
            vertices_triangle.push_back(vertices[((i + 1) * nb_vertex) + j + 1]);
            vertices_triangle.push_back(vertices[(i * nb_vertex) + j + 1]);

            n = cross(vertices[((i + 1) * nb_vertex) + j + 1] - vertices[(i * nb_vertex) + j],
                vertices[(i * nb_vertex) + j + 1] - vertices[(i * nb_vertex) + j]);

            normals.push_back(n);
            normals.push_back(n);
            normals.push_back(n);
        }

    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    unsigned int time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "Time for bezier surface: " << time << "ms" << std::endl;
}
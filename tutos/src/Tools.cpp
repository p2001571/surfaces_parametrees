#include "tutos/include/Tools.h"

double factorielle(double n)
{
    double res = 1;

    while (n > 1)
    {
        res *= n;
        n--;
    }

    return res;
}

double combinaison(double m, double i)
{
    return ((double)factorielle(m)) / (factorielle(i) * factorielle(m - i));
}

double bernstein(double combinaison_m_i, const double& m, const double& i, const double& u)
{
    double a = combinaison_m_i;
    double b = powf(u, i);
    double c = powf((double)1.0f - u, m - i);
    return a * b * c;
}


Point cross(Point& a, Point& b)
{
    return Point(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
}
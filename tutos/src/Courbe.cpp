#include "../include/Courbe.h"
#include <tutos/include/Tools.h>

Courbe::Courbe(std::vector<Point> points_controle)
{
    this->points_controle = points_controle;
    nb_points = points_controle.size();
}

Point Courbe::pointAtT(double t)
{
    Point result(0, 0, 0);

    for (int i = 0; i < nb_points; i++)
        result = result + points_controle[i] * bernstein(combinaisons_courbes[i], nb_points - 1, i, t);

    return result;
}

Point Courbe::tangenteAtT(double t)
{
    Point tangente(0, 0, 0);

    for (int i = 0; i < nb_points - 1; i++)
    {
        tangente = tangente + nb_points *
            bernstein(combinaisons_courbes[i], nb_points - 1, i, t)
            * Point(points_controle[i + 1] - points_controle[i]);
    }

    return tangente;
}
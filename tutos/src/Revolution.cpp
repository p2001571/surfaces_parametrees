#include "../include/Revolution.h"


double length(Point& p)
{
    return sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
}

void normalize(Point& p)
{
    double l = length(p);
    p.x /= l;
    p.y /= l;
    p.z /= l;
}

Revolution::Revolution()
{
    std::vector<Point> points_controle;

    for (int i = 0; i < 10; i++)
    {
        float x = rand() % 10;
        float y = cos((float)i / 2) * 3;
        float z = i;
        points_controle.push_back(Point(x, y, z));
    }

    c = Courbe(points_controle);
    Polygonize();
    initBuffers();
}

Revolution::Revolution(std::vector<Point> points_controle)
{
    c = Courbe(points_controle);
    Polygonize();
}

void Revolution::initBuffers()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo_vertices);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);

    // sommets
    glBufferData(
        GL_ARRAY_BUFFER,
        vertices_triangle.size() * sizeof(vec3),
        &vertices_triangle.front().x,
        GL_STATIC_DRAW
    );

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    // normales
    glGenBuffers(1, &vbo_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);

    glBufferData(
        GL_ARRAY_BUFFER,
        normals_triangle.size() * sizeof(vec3),
        &normals_triangle.front().x,
        GL_STATIC_DRAW
    );

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    shader_program = read_program("tutos/shader.glsl");
    program_print_errors(shader_program);
}

float Revolution::equationRadialeEtoile(float angle)
{
    return (int)(angle * 100) % 2;
}

float Revolution::equationRadialeFleur(float angle)
{
    return 0.5 + abs(cos(angle*3));
}

float Revolution::equationRadialeSmooth(float angle)
{
    return 1.0f;
}

void Revolution::Polygonize()
{
    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

    std::vector<vec3> vertices;
    std::vector<vec3> normals;
    vertices.resize(nb_vertex_courbe * nb_vertex_radial);
    normals.resize(nb_vertex_courbe * nb_vertex_radial);

    for (int i = 0; i < nb_vertex_courbe; i++)
    {
        float u = (float)i / (nb_vertex_courbe - 1);
        Point cu = c.pointAtT(u);
        Point t = c.tangenteAtT(u);
        Point n = cross(Point(0, 1, 0), t);
        normalize(t);
        normalize(n);

        Point b = cross(t, n);
        normalize(b);

        for (int j = 0; j < nb_vertex_radial; j++)
        {
            float v = (float)j / (nb_vertex_radial - 1);
            Point p_u_v = cu + equationRadialeFleur(2 * M_PI * v) * (cos(2 * M_PI * v) * n + sin(2 * M_PI * v) * b);
            vec3 vertex(p_u_v.x, p_u_v.y, p_u_v.z);
            vertices[i * nb_vertex_courbe + j] = vertex;
            normals[i * nb_vertex_courbe + j] = p_u_v - cu;
        }
    }

    // creation des triangles
    for (int i = 0; i < nb_vertex_courbe - 1; i++)
        for (int j = 0; j < nb_vertex_radial - 1; j++)
        {
            vertices_triangle.push_back(vertices[i * nb_vertex_courbe + j]);
            vertices_triangle.push_back(vertices[i * nb_vertex_courbe + j + 1]);
            vertices_triangle.push_back(vertices[(i + 1) * nb_vertex_courbe + j]);

            normals_triangle.push_back(normals[i * nb_vertex_courbe + j]);
            normals_triangle.push_back(normals[i * nb_vertex_courbe + j + 1]);
            normals_triangle.push_back(normals[(i + 1) * nb_vertex_courbe + j]);

            vertices_triangle.push_back(vertices[(i + 1) * nb_vertex_courbe + j]);
            vertices_triangle.push_back(vertices[(i + 1) * nb_vertex_courbe + j + 1]);
            vertices_triangle.push_back(vertices[i * nb_vertex_courbe + j + 1]);

            normals_triangle.push_back(normals[(i + 1) * nb_vertex_courbe + j]);
            normals_triangle.push_back(normals[(i + 1) * nb_vertex_courbe + j + 1]);
            normals_triangle.push_back(normals[i * nb_vertex_courbe + j + 1]);
        }

    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    unsigned int time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "Time for revolution surface: " << time << "ms" << std::endl;
}

void Revolution::draw(Transform& vp)
{
    glUseProgram(shader_program);
    glBindVertexArray(vao);

    program_uniform(shader_program, "vpMatrix", vp);
    glDrawArrays(GL_TRIANGLES, 0, vertices_triangle.size());
}

Revolution::~Revolution()
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo_vertices);
    glDeleteBuffers(1, &vbo_normals);
    release_program(shader_program);
}